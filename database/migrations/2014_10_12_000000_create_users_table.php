<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('nombre1');
			$table->string('nombre2');
			$table->string('apellido1');
			$table->string('apellido2');
			$table->integer('id_tipo_doc');
			$table->bigInteger('num_doc')->unique();
			$table->string('email');
			$table->bigInteger('tel');
			$table->string('usuario');
			$table->string('password', 60);
			$table->integer('level');
			$table->string('id_formacion');
			$table->string('id_ficha');

			$table->rememberToken();
			$table->timestamps();
		});
	}


public function validator(array $data){
	return validator::make($data, ['name' => 'required|max:255',
		email => 'required|email|max:255|unique:users']);
}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
