<?php namespace spc\Http\Controllers;

use spc\Http\Controllers\Controller;
use Request;
use Auth;

class UserLogin extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

// obtiene los datos de las celdas
	public function postIndex()	{
		$userdata = array(
			'name' => Request::input('num_doc'),
			'password' => Request::input('password')
			);
// autentifica al usuario
		if (Auth::attempt($userdata)) {
			# code...
			if (Auth::user()->level == 1) {
				return redirect('administrador');
			} else {
				return redirect('/');
			}
			// return $userdata;
		} else {
			return redirect('login');
		}
	}
}
