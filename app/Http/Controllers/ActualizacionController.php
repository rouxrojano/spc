<?php namespace spc\Http\Controllers;

use spc\Http\Requests;
use spc\Http\Controllers\Controller;
use spc\User as User;
use Illuminate\Http\Request;
use View;
use findOrFail;

class ActualizacionController extends Controller {

	public function __construct(){
		$this->middleware('auth');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function index(){
//
	}

	public function create(){
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		//para crear un usuario

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	// imprime el formulario
	public function edit($id){
		$user=User::findOrFail($id); //para cargar el usuario
		return view('actualizacion', compact('user'));

	}

	public function update($id)	{
		$user=User::findOrFail($id);
		$user->fill(Request::all());
		$user->save();
		return view('actualizacion');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		//
	}

}
