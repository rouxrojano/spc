<?php namespace spc\Http\Controllers;

use spc\Http\Requests;
use spc\Http\Controllers\Controller;
use Request;
use Auth;
use spc\User as User;
use spc\Id_ficha as Id_ficha;
use Hash;


class RegistroController extends Controller {

// restrinciòn sino estas logeado
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$fichas = Id_ficha::all();
		return view('admin.usuario')
			->with('fichas',$fichas)
			->with('msg','false');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){

		$name=Request::input('name');

		$user = new User;
		$user->num_doc = $name;
		$user->name = $name;
		$user->id_tipo_doc = Request::input('id_tipo_doc');
		$user->nombre1 = Request::input('nombre1');
		$user->nombre2 = Request::input('nombre2');
		$user->apellido1 = Request::input('apellido1');
		$user->apellido2 = Request::input('apellido2');
		$user->tel = Request::input('tel');
		$user->email = Request::input('email');
		$user->usuario = Request::input('usuario');
		$user->password = Hash::make(Request::input('password'));
		$user->id_formacion = Request::input('id_formacion');
		$user->id_ficha = Request::input('id_ficha');


		// return $user;
		$user->save();

		$fichas = Id_ficha::all();
		$msg = 'true';
		return view('admin.usuario')
			->with('fichas',$fichas)
			->with('msg',$msg);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
