<?php namespace spc\Http\Controllers;

use spc\Http\Requests;
use spc\Http\Controllers\Controller;
use Request;
use Auth;
use spc\User as User;
// use Illuminate\Support\Facades\Session;
class DatosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()	{
		$datos = User::all();
		$msg = 'false';
		return view('admin.listado')
		->with('datos',$datos);
	}


	public function destroy($id){
		$datos = User::all();
		User::destroy($id);
		$msg = 'true';

		return redirect('funciones-del-administrador/listado-de-los-aprendices')->with('msg',$msg);

		// Session::flash('message','El registro del usuario fue eliminado' );
		// redirect()->view('admin.listado')
		// ->with('datos',$datos)
		// ->with('msg',$msg);

	}


}
