<?php


// administrador
Route::controller('userlogin', 'UserLogin');

// indica si el usuario esta registrado puede visualizar la pàgina home, sino 
//se regresara al login
Route::get('logout', function(){
	Auth::logout();
	return redirect('login');
});

// controladores

//controlador que maneja el control de que el usuario ingrese
Route::get('login',function(){return view('auth.login');}); 

//controlador que maneja el registro de los aprendices
Route::resource('funciones-del-administrador/registro', 'RegistroController'); 

//controlador que maneja la actualizacion del registro de los aprendices
Route::resource('actualizacion-de-aprendices', 'ActualizacionController'); 

// controlador del listado de los aprendices registrados
Route::resource('funciones-del-administrador/listado-de-los-aprendices', 'DatosController'); 

// para que no entre a home sino esta logeado
Route::get('/',									
	function(){
		if (Auth::check()) {
			return view('home');
		} else {
			return redirect('login');
		}
	}
);
// 

// validacion

// validacion
Route::get('actualizacion',											function(){return view('edit');});
Route::get('administrador',											function(){return view('admi');});
Route::get('aprendices',											function(){return view('aprendiz');});
Route::get('funciones-del-administrador',							function(){return view('admin.funciones');});
Route::get('funciones-del-administrador/listado-de-los-preguntas',	function(){return view('admin.preguntas');});
Route::get('listado',												function(){return view('admin.listado');});




Route::get('cime1',                             function(){return view('cime1');});
Route::get('procesos',                          function(){return view('procesos');});
Route::get('proceso-de-preliminares',           function(){return view('preliminares');});
Route::get('equipos-necesarias-para-una-obra',	function(){return view('equipos');});

// fin de principal

// cimentacion
Route::get('proceso-de-cimentacion',            function(){return view('cimentacion');});
// fin de cimentacion

// ciudades
Route::get('ciudad/bd-bacata',                         function(){return view('bd');});
Route::get('ciudad/biblioteca-publica-virgilio-barco', function(){return view('biblioteca');});
Route::get('ciudad/puente-baluarte-bicentenario',      function(){return view('baluarte');});
Route::get('ciudad/casa-de-cultura-del-mundo',         function(){return view('casa');});
Route::get('ciudad/la-casa-de-la-cascada',             function(){return view('cascada');});
Route::get('ciudad/burj-al-arab',                      function(){return view('dubai');});
Route::get('ciudad/coliseo-romano',                    function(){return view('coliseo');});
Route::get('ciudad/eurotunel',                    		function(){return view('eurotunel');});
Route::get('ciudad/hotel-ryugyong',                    function(){return view('ryugyong');});
Route::get('ciudad/shanghai-world-financial-center',   function(){return view('shanghai');});
Route::get('ciudad/hotel-turning-torso',               function(){return view('turning');});
Route::get('ciudad/el-viaducto-de-millau',             function(){return view('viaducto');});
Route::get('ciudad/la-gran-muralla-china',                     function(){return view('muralla');});
Route::get('materiales',                        function(){return view('materiales');});
// fin de ciudades


// preliminares
Route::get('proceso-de-preliminares/instalacion-de-campamento',         function(){return view('campamento');});
Route::get('proceso-de-preliminares/cerramiento-de-obra',               function(){return view('cerramiento');});
Route::get('proceso-de-preliminares/instalaciones-de-una-obra',         function(){return view('instalaciones');});
Route::get('proceso-de-preliminares/limpieza-y-descapote-de-una-obra',  function(){return view('limpieza');});
Route::get('proceso-de-preliminares/localizacion-y-replanteo',          function(){return view('localizacion');});
Route::get('proceso-de-preliminares/movimiento-de-tierra',              function(){return view('movimiento');});
Route::get('proceso-de-preliminares/nivelacion-de-terreno',				function(){return view('nivelacion');});
Route::get('proceso-de-preliminares/demolicion-de-obra',                function(){return view('demolicion');});
Route::get('proceso-de-preliminares/drenaje-de-una-obra',               function(){return view('drenaje');});
// fin de preliminares

// ciudad
Route::get('ciudad',                          function(){return view('ciudad');});
// fin ciudad
Route::get('maquinas-necesarias-para-una-obra', function(){return view('maquinas');});


// herramientas
Route::get('clasificacion-de-las-herramientas', function(){return view('herramientas');});
Route::get('clasificacion-de-las-herramientas/herramienta-de-acabado',            function(){return view('h-acabado');});
Route::get('clasificacion-de-las-herramientas/herramienta-de-agujero',            function(){return view('h-agujero');});
Route::get('clasificacion-de-las-herramientas/herramienta-de-corte',              function(){return view('h-corte');});
Route::get('clasificacion-de-las-herramientas/herramienta-de-excavacion',         function(){return view('h-excavacion');});
Route::get('clasificacion-de-las-herramientas/herramienta-de-medicion',           function(){return view('h-medicion');});
Route::get('clasificacion-de-las-herramientas/herramienta-de-sujecion',           function(){return view('h-sujecion');});
Route::get('clasificacion-de-las-herramientas/herramienta-de-trocear',            function(){return view('h-trocear');});
// fin de herramientas


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	]);
